/* 
 * File:   rf_transceiver.cpp
 * Author: stei
 * 
 * Created on October 4, 2013, 7:23 PM
 */

#include "rf_transceiver.h" 

rf_transceiver::rf_transceiver() {
}

rf_transceiver::~rf_transceiver() {
}

int rf_transceiver::init() {
    unsigned char string, para[19];
    int i, size;

    pinMode(SET_PORT, OUTPUT);

    //serial init
    if (RS232_OpenComport(SERIAL_PORT, SET_BAUD_RATE, &fd)) {
        return FAILED;
    }

    digitalWrite(SET_PORT, LOW);
    usleep(100000);

    //sending request settings
    RS232_cputs(SERIAL_PORT, "RD\r\n");
    usleep(100000);

    i = 0;
    while (size = RS232_PollComport(SERIAL_PORT, &string, sizeof (char)) > 0) {
        if (i < 19) {
            para[i] = string;
        }
        i++;
    }
    //activate RF to running mode
    digitalWrite(SET_PORT, HIGH);

    //parsing return value from RF
    int fr_temp = 0;
    int pw_temp = 0;
    for (i = 5; i < 11; i++) {
        fr_temp = (fr_temp * 10) + (para[i] - 48);
    }
    pw_temp = para[14] - 48;

    RS232_CloseComport(SERIAL_PORT);
    if (RS232_OpenComport(SERIAL_PORT, BAUD_RATE, &fd)) {
        return FAILED;
    }

    // validate return value
    if ((para[0] == 'P') && (para[1] == 'A') && (para[2] == 'R') && (para[3] == 'A')) {
        this->frequency = fr_temp;
        this->power = pw_temp;
        return SUCCESS;
    } else
        return FAILED;
}

int rf_transceiver::transmit(unsigned char * data, int data_length) {
    return RS232_SendBuf(SERIAL_PORT, data, data_length);
}

int rf_transceiver::receive(unsigned char * data, int data_length) {
    return RS232_PollComport(SERIAL_PORT, data, data_length);
}

int rf_transceiver::setFrequency(int freq) {
    if (freq < 418000 || freq > 455999)
        return INPUT_ERROR;
    return changeRFSettings(freq, this->power);
}

int rf_transceiver::getFrequency() {
    return this->frequency;
}

int rf_transceiver::setPower(int power) {
    if (power < 1 || power > 9)
        return INPUT_ERROR;
    return changeRFSettings(this->frequency, power);
}

int rf_transceiver::getPower() {
    return this->power;
}

void rf_transceiver::pause() {
    digitalWrite(SET_PORT, LOW);
}

void rf_transceiver::resume() {
    digitalWrite(SET_PORT, HIGH);
}
int rf_transceiver::handle() {
    return this->fd;
}

int rf_transceiver::changeRFSettings(int freq, int power) {
    //local vars
    unsigned char respond, para[19];
    char settings[19];
    int i = 0;

    RS232_CloseComport(SERIAL_PORT);
    if (RS232_OpenComport(SERIAL_PORT, SET_BAUD_RATE, &fd)) {
        return FAILED;
    }

    //start settings
    digitalWrite(SET_PORT, LOW);
    usleep(100000);

    //create settings
    sprintf(settings,
            "WR %d %d %d %d 0\r\n",
            freq,
            RF_DATA_RATE,
            power,
            UART_RATE);

    //display settings
    RS232_cputs(SERIAL_PORT, settings);
    usleep(100000);

    //get return value
    while (RS232_PollComport(SERIAL_PORT, &respond, sizeof (char))) {
        if (i < 19) {
            para[i] = respond;
        }
        i++;
    }

    //activate RF to running mode
    digitalWrite(SET_PORT, HIGH);

    //parsing return value from RF
    int fr_temp = 0;
    int pw_temp = 0;
    for (i = 5; i < 11; i++) {
        fr_temp = (fr_temp * 10) + (para[i] - 48);
    }
    pw_temp = para[14] - 48;

    RS232_CloseComport(SERIAL_PORT);
    if (RS232_OpenComport(SERIAL_PORT, BAUD_RATE, &fd)) {
        return FAILED;
    }
    
    // validate return value
    if (fr_temp != freq)
        return FAILED;
    else
        this->frequency = fr_temp;

    if (pw_temp != power)
        return FAILED;
    else
        this->power = pw_temp;

    return SUCCESS;
}

