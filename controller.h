/* 
 * File:   controller.h
 * Author: Fadhlullah
 *
 * Created on 09 October 2013, 14:34
 */

#ifndef CONTROLLER_H
#define	CONTROLLER_H

/* Include GPIO and LCD Library for Raspberry */
#include <wiringPi.h>
#include <lcd.h>
#include <pthread.h>

/* Include Object Class */
#include "rf_transceiver.h"
#include "soundcard.h"
#include "codec.h"
#include "masterkey.h"
#include "SecKeyTransport.h"
#include "Srtp.h"
#include "Times.h"

/* Define Bit Operational Function */
#define SETBIT(ADDRESS,BIT) (ADDRESS |= (1<<BIT)) 
#define CLEARBIT(ADDRESS,BIT) (ADDRESS &= ~(1<<BIT))
#define CHECKBIT(ADDRESS,BIT) (ADDRESS & (1<<BIT))

/* Define Return Value Code */
#define FAILED (-2)
#define SUCCESS 0

/* Define Flag Name */
#define STATE_CHANGED_FLAG 0
#define TX_FLAG 1
#define RX_FLAG 2
#define SECURE_FLAG 3
#define SET_FREQ_FLAG 4
#define SET_KEY_FLAG 5
#define SET_POWER_FLAG 6
#define KEY_READY_FLAG 7
#define GEN_KEY_FLAG 8
#define HOST_FLAG 9
#define SECURE_LOCK_FLAG 10

/* Define WiringPi Pin Number for ScanKey */
#define ROW1 2
#define ROW2 3
#define ROW3 4
#define ROW4 5
#define COLUMN1 8
#define COLUMN2 9
#define COLUMN3 7
#define COLUMN4 0

class controller {
public:
    controller();
    int init(rf_transceiver * rf, soundcard * sc, codec * cdc, masterkey * mk, Srtp * srtp, SecKeyTransport * skt);
    void control(pthread_mutex_t * set_mutex, pthread_mutex_t * lock_mutex, pthread_cond_t * set_cond, pthread_cond_t * lock_cond);
    void processing(pthread_mutex_t * sc_mutex, pthread_mutex_t * rf_mutex, pthread_cond_t * sc_cond, pthread_cond_t * rf_cond);
    void sampling(pthread_mutex_t * sc_mutex, pthread_cond_t * sc_cond);
    void transmitting(pthread_mutex_t * rf_mutex, pthread_cond_t * rf_cond);
    void setting(pthread_mutex_t * mutex, pthread_cond_t * cond);
    void secure_lock(pthread_mutex_t * mutex, pthread_cond_t * cond);
    int lcd_handle;

private:
    /* Object Binder */
    rf_transceiver * rf_handle;
    soundcard * sc_handle;
    codec * cd_handle;
    masterkey * mk_handle;
    Srtp * srtp_handle;
    SecKeyTransport * skt_handle;

    /* Helper Method */
    char keyhandler();
    char scankey();
    static char num(char input);
    void changestate(char state_no, char ministate_no, int * count);
    static void display(char state_no, char ministate_no);

    /* Static Local Variable */
    char state;
    char ministate;
    char dstate;
    char prev_out;
    int pass_attempt;

    /* Shared Variable */
    short mode;
    int temp;
    int setcode;
    int index;
    SAMPLE buffer_tx[SAMPLE_PER_FRAME];
    SAMPLE buffer_rx[SAMPLE_PER_FRAME];
    unsigned char cipher[SECURED_BYTE_PER_FRAME];
};

#endif	/* CONTROLLER_H */

