/* 
 * File:   masterkey.h
 * Author: stei
 *
 * Created on November 4, 2013, 2:13 PM
 */

#ifndef MASTERKEY_H
#define	MASTERKEY_H

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <fstream>

#include <cryptopp/osrng.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/aes.h>
#include <cryptopp/filters.h>
#include <cryptopp/modes.h>
#include <cryptopp/sha.h>

//define return value
#define INPUT_ERROR (-3)
#define FAILED (-2)
#define TIMEOUT (-1)
#define SUCCESS 0

#define AES_CBC_SALT "3E058414A34CBA6783E45FB50E37812D1453D13B3993AA3360A9101168B973A589A7ED9A6CB5FD4295AECC98564CBA5B7D0FC183C68E33421653B9736F21B1AA"
#define AES_CBC_KEY_LENGTH (16)
#define AES_CBC_SALT_LENGTH (16)
#define NUMBER_OF_KEYS (1000)

#define CHIPER_LENGTH (64)
#define MASTER_KEY_LENGTH (52)
#define ENCRYPTION_KEY_LENGTH (16)
#define SALT_KEY_LENGTH (16)
#define AUTHENTICATION_KEY_LENGTH (20)

#define DEVICE_ID_PASSWORD "j54a5adad4545da54d45df5"
#define DEVICE_ID_FILENAME "ddad3434sf2cwv4.id"
#define ID_CHIPER_LENGTH (32)

#define MASTER_FILENAME "najkcjkabuyhbac54.56uyg5756y"
#define UPLOAD_DIRECTORY "/home/pi/6541845kfadaop/092387jsvjkn476gb729/"

using namespace std;
using namespace CryptoPP;

class masterkey {
public:
    masterkey();
    masterkey(const masterkey& orig);
    virtual ~masterkey();
    bool input_password(int password_int);
    string getMasterKey(long key_id);
    u_int8_t getDeviceID();
    static byte * parseEncryptionKey(string master_key);
    static byte * parseSaltKey(string master_key);
    static byte * parseAuthenticationKey(string master_key);
    static string hex_encoder(string decoded);
    static string hex_decoder(string encoded);
    static void generateRandom(byte * data, int size);
private:
    string key_location;
    string password;
    bool check_password();
    static string sha_512(string source);
    static string master_key_hash(string master_key);
    static string aes_cbc_decode(string cipher, string password_hash);
};

#endif	/* MASTERKEY_H */

