/* 
 * File:   SecKeyTransport.cpp
 * Author: gian nurman
 * 
 * Created on 05 November 2013, 14:42
 */

#include "SecKeyTransport.h"


SecKeyTransport::SecKeyTransport() {

}

SecKeyTransport::~SecKeyTransport() {

}

int SecKeyTransport::Init(string master_key) {
    byte *temp = masterkey::parseEncryptionKey(master_key);
        
    int i;
    for (i = 0; i < 16; i++) {
        this->master_key.enc[i] = temp[i];
    }    
    temp = masterkey::parseSaltKey(master_key);
    for (i = 0; i < 16; i++) {
        this->master_key.salt[i] = temp[i];
    }
    temp= masterkey::parseAuthenticationKey(master_key);
    for (i = 0; i < 20; i++) {
        this->master_key.auth[i] = temp[i];
    }
    return SUCCESS;
}

int SecKeyTransport::Init(master_key_t master_key) {
    int i;
    for (i = 0; i < 16; i++) {
        this->master_key.enc[i] = master_key.enc[i];
    }
    for (i = 0; i < 16; i++) {
        this->master_key.salt[i] = master_key.salt[i];
    }
    for (i = 0; i < 20; i++) {
        this->master_key.auth[i] = master_key.auth[i];
    }
}

int SecKeyTransport::Init(byte enc_master_key[16], byte salt_master_key[16], byte auth_master_key[20]) {
    int i;
    for (i = 0; i < 16; i++) {
        this->master_key.enc[i] = enc_master_key[i];
    }
    for (i = 0; i < 16; i++) {
        this->master_key.salt[i] = salt_master_key[i];
    }
    for (i = 0; i < 20; i++) {
        this->master_key.auth[i] = auth_master_key[i];
    }
}

int SecKeyTransport::Protect(byte secure_key_packet_out[42], byte enc_session_key_in[16], byte salt_session_key_in[16]) {
    //byte abMac[HMAC::DIGESTSIZE];
    int i = 0;
    byte plain_payload[32];
    byte secure_payload[32];

    for (i = 0; i < 16; i++) {
        plain_payload[i] = enc_session_key_in[i];
    }

    for (i = 0; i < 16; i++) {
        plain_payload[i + 16] = salt_session_key_in[i];
    }

    CBC_Mode<AES>::Encryption encryptor;
    encryptor.SetKeyWithIV(this->master_key.enc, 16, this->master_key.salt);

    encryptor.ProcessData(secure_payload, plain_payload, 32);

    //RSA-512	 
    HMAC<SHA1> auth(master_key.auth, 20);

    byte hash[10];
    auth.CalculateTruncatedDigest(hash, 10, secure_payload, 32);

    for (i = 0; i < 32; i++) {
        secure_key_packet_out[i] = secure_payload[i];
        this->secure_key_packet[i] = secure_payload[i];
    }

    for (i = 0; i < 10; i++) {
        secure_key_packet_out[i + 32] = hash[i];
        this->secure_key_packet[i + 32] = hash[i];
    }

    return SUCCESS;
}

int SecKeyTransport::Protect(byte secure_key_packet_out[42], session_key_t session_key_in) {
    return Protect(secure_key_packet_out, session_key_in.enc, session_key_in.salt);
}

int SecKeyTransport::Unprotect(byte enc_session_key_out[16], byte salt_session_key_out[16], byte secure_key_packet_in[42]) {
    int i = 0;
    byte plain_payload[32];
    byte secure_payload[32];

    for (i = 0; i < 32; i++) {
        secure_payload[i] = secure_key_packet_in[i];
    }

    byte hash[10];
    for (i = 0; i < 10; i++) {
        hash[i] = secure_key_packet_in[i + 32];
    }

    HMAC< SHA1 > auth(this->master_key.auth, 20);
    if (auth.VerifyTruncatedDigest(hash, 10, secure_payload, 32)) {
        //cout << "\n\rPacket Verified\n\r" << endl;
        CBC_Mode< AES >::Decryption decryptor;
        decryptor.SetKeyWithIV(this->master_key.enc, 16, this->master_key.salt);
        decryptor.ProcessData(plain_payload, secure_payload, 32);

        for (i = 0; i < 16; i++) {
            enc_session_key_out[i] = plain_payload[i];
        }
        for (i = 0; i < 16; i++) {
            salt_session_key_out[i] = plain_payload[i + 16];
        }
        return SUCCESS;
    } else {
        //        cout << "\n\rPacket Unverified\n\r" << endl;
        for (i = 0; i < 16; i++) {
            enc_session_key_out[i] = 0;
        }
        for (i = 0; i < 16; i++) {
            salt_session_key_out[i] = 0;
        }
        return FAILED;
    }
}

int SecKeyTransport::Unprotect(session_key_t &session_key_out, byte secure_key_packet_in[42]) {
    return Unprotect(session_key_out.enc, session_key_out.salt, secure_key_packet_in);
}
