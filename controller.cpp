/* 
 * File:   controller.cpp
 * Author: Fadhlullah
 * 
 * Created on 09 October 2013, 14:34
 */

#include "controller.h"

/* Constructor Method */
controller::controller() {
    /* Initialization Private Variable */
    this->mode = 0;
    this->state = 0;
    this->dstate = 0;
    this->ministate = 0;
    this->prev_out = 0;
    this->pass_attempt = 1;
    SETBIT(this->mode, RX_FLAG);
    SETBIT(this->mode, STATE_CHANGED_FLAG);
}

/* Initialization Method */
int controller::init(rf_transceiver * rf, soundcard * sc, codec * cdc, masterkey * mk, Srtp * srtp, SecKeyTransport * skt) {
    int i = 0, code = FAILED;

    printf("ID = %d\n", mk_handle->getDeviceID());

    /* Bind Other Class with Private Object */
    this->rf_handle = rf;
    this->sc_handle = sc;
    this->cd_handle = cdc;
    this->mk_handle = mk;
    this->srtp_handle = srtp;
    this->skt_handle = skt;

    /* Initialization GPIO Library */
    wiringPiSetup();

    /* Set GPIO Direction for LCD */
    pinMode(14, OUTPUT);
    pinMode(13, OUTPUT);
    pinMode(12, OUTPUT);
    pinMode(6, OUTPUT);

    /* Set GPIO Direction for Keypad */
    pinMode(0, OUTPUT);
    pinMode(7, OUTPUT);
    pinMode(8, OUTPUT);
    pinMode(9, OUTPUT);
    pinMode(2, INPUT);
    pinMode(4, INPUT);
    pinMode(5, INPUT);
    pinMode(3, INPUT);

    /* Set Pull Down for Input GPIO */
    pullUpDnControl(3, PUD_DOWN);
    pullUpDnControl(2, PUD_DOWN);
    pullUpDnControl(4, PUD_DOWN);
    pullUpDnControl(5, PUD_DOWN);

    /* Initialization GPIO for LCD */
    this->lcd_handle = lcdInit(2, 16, 4, 11, 10, 12, 13, 14, 6, 0, 0, 0, 0);

    lcdClear(this->lcd_handle);
    lcdPrintf(this->lcd_handle, "Starting");

    /* Initialization RF Transceiver */
    while ((SUCCESS != code) && (i != 3)) {
        code = this->rf_handle->init();
        i++;
    }

    lcdPrintf(this->lcd_handle, " .");

    /* Initialization Codec */
    if (code == SUCCESS) {
        code = FAILED;
        i = 0;
        while ((SUCCESS != code) && (i != 3)) {
            code = this->cd_handle->init();
            i++;
        }
    }

    delay(300);
    lcdPrintf(this->lcd_handle, " .");

    /* Initialization SoundCard */
    if (code == SUCCESS) {
        code = FAILED;
        i = 0;
        while ((SUCCESS != code) && (i != 3)) {
            code = this->sc_handle->init();
            i++;
        }
    }

    delay(300);
    lcdPrintf(this->lcd_handle, " .");
    delay(300);

    /* Return Value Code */
    return code;
}

/* Scanning Key Method */
char controller::scankey() {
    /* 4th Column */
    digitalWrite(COLUMN1, 0);
    digitalWrite(COLUMN2, 0);
    digitalWrite(COLUMN3, 0);
    digitalWrite(COLUMN4, 1);
    if (digitalRead(ROW1) == 1) return 4;
    if (digitalRead(ROW2) == 1) return 8;
    if (digitalRead(ROW3) == 1) return 12;
    if (digitalRead(ROW4) == 1) return 16;

    /* 1st Column */
    digitalWrite(COLUMN1, 1);
    digitalWrite(COLUMN2, 0);
    digitalWrite(COLUMN3, 0);
    digitalWrite(COLUMN4, 0);
    if (digitalRead(ROW1) == 1) return 1;
    if (digitalRead(ROW2) == 1) return 5;
    if (digitalRead(ROW3) == 1) return 9;
    if (digitalRead(ROW4) == 1) return 13;

    /* 2nd Column */
    digitalWrite(COLUMN1, 0);
    digitalWrite(COLUMN2, 1);
    digitalWrite(COLUMN3, 0);
    digitalWrite(COLUMN4, 0);
    if (digitalRead(ROW1) == 1) return 2;
    if (digitalRead(ROW2) == 1) return 6;
    if (digitalRead(ROW3) == 1) return 10;
    if (digitalRead(ROW4) == 1) return 14;

    /* 3rd Column */
    digitalWrite(COLUMN1, 0);
    digitalWrite(COLUMN2, 0);
    digitalWrite(COLUMN3, 1);
    digitalWrite(COLUMN4, 0);
    if (digitalRead(ROW1) == 1) return 3;
    if (digitalRead(ROW2) == 1) return 7;
    if (digitalRead(ROW3) == 1) return 11;
    if (digitalRead(ROW4) == 1) return 15;

    /* Return 0 Id No Key Pressed */
    return 0;
}

/* Method to handle Debouncing and Auto-repeat */
char controller::keyhandler() {
    char in = scankey();
    static int dcount;

    if (this->dstate == 0) { //Not Pressed
        if (in != 0) {
            this->dstate = 1;
            this->prev_out = in;
        } else
            this->dstate = 0;
    } else if (this->dstate == 1) { //Pressed
        if (in == 0) {
            this->dstate = 0;
        } else if (in == this->prev_out) {
            this->dstate = 2;
            dcount = 0;
        } else {
            this->dstate = 1;
            this->prev_out = in;
        }
    } else if (this->dstate == 2) { //Holded
        if (in == 0) {
            this->dstate = 0;
            this->prev_out = 0;
        } else if (in == this->prev_out) {
            dcount++;
            this->dstate = 2;
        }
    }

    if (this->dstate == 0) {
        return 0;
    } else if (this->dstate == 1) {
        return in;
    } else if (this->dstate == 2)
        if (in == 13) {
            if (dcount == 20)
                return 20;
            else
                return 0;
        } else if (this->state == 0 || this->state == 1) { //Transmit (A) pressed
            if (in == 4)
                return in;
            else
                return 0;
        } else if (this->state == 2) { //Autorepeat Volume Setting
            if (in == 4 || in == 8)
                if ((dcount > 14) && ((dcount - 5) % 10) == 0)
                    return in;
                else
                    return 0;
            else
                return 0;
        } else if (this->state == 4) { //Autorepeat Delete
            if (in == 4)
                if ((dcount > 14) && ((dcount - 5) % 10) == 0)
                    return in;
                else
                    return 0;
            else
                return 0;
        } else if (this->state == 6) { //Autorepeat Power Setting
            if (in == 4 || in == 8)
                if ((dcount > 14) && ((dcount - 5) % 10) == 0)
                    return in;
                else
                    return 0;
            else
                return 0;
        } else
            return 0;
}

/* Method to Convert keyCode into number */
char controller::num(char input) {
    switch (input) {
            //'0' to '9' pressed
        case 1: case 2: case 3:
            return input;
            break;
        case 5: case 6: case 7:
            return input - 1;
            break;
        case 9: case 10: case 11:
            return input - 2;
            break;
        case 14:
            return 0;
            break;
            //'A' (Delete) pressed
        case 4:
            return 11;
            break;
            //Nothing pressed
        default:
            return 10;
    }
}

/* Method to Change State */
void controller::changestate(char state_no, char ministate_no, int * count) {
    this->state = state_no;
    this->ministate = ministate_no;
    *count = 0;
    SETBIT(this->mode, STATE_CHANGED_FLAG);
}

/* FSM for Controller */
void controller::control(pthread_mutex_t * set_mutex, pthread_mutex_t * lock_mutex, pthread_cond_t * set_cond, pthread_cond_t * lock_cond) {
    static int count, cursor;
    char input = keyhandler();

    if (input == 20) {
        changestate(7, 0, &count);
    } else if (this->state == 0) {//not secured
        if (input == 8) { //Secure On (B)
            if CHECKBIT(this->mode, SECURE_LOCK_FLAG) {
                changestate(0, 2, &count);
            } else {
                changestate(CHECKBIT(this->mode, KEY_READY_FLAG) ? 1 : 5, 0, &count);
            }
        } else if (input == 12) { //Enter Setting (C)
            changestate(3, 0, &count);
        } else if (input == 16) { //Volume Setting (D)
            changestate(2, 0, &count);
        } else if (this->ministate == 0) { //Receive
            if (input == 4)
                changestate(0, 1, &count);
        } else if (this->ministate == 1) { //Transmit
            if (input != 4)
                changestate(0, 0, &count);
        } else if (this->ministate == 2) { //Display "Secure this->mode lock"
            if (count > 20)
                changestate(0, 0, &count);
            else
                count++;
        }
    } else if (this->state == 1) { //secured
        if (input == 8) { //Secure Off (B)
            changestate(0, 0, &count);
        } else if (input == 12) { //Enter Setting (C)
            changestate(3, 0, &count);
        } else if (input == 16) { //Volume Setting (D)
            changestate(2, 0, &count);
        } else if (this->ministate == 0) { //Receive
            if (input == 4) {
                changestate(1, 1, &count);
            }
        } else if (this->ministate == 1) { //Transmit
            if (input != 4) {
                changestate(1, 0, &count);
            }
        }
    } else if (this->state == 2) { //volume setting
        if (input == 16) { //Back (D)
            changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
        } else if (this->ministate == 0) {
            if (input == 4) { //Volume Up (A)
                changestate(2, 1, &count);
            } else if (input == 8) { //Volume Down (B)
                changestate(2, 2, &count);
            } else if (count > 60) {
                changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
            } else
                count++;
        } else if (this->ministate == 1) { //Volume Up
            changestate(2, 0, &count);
        } else if (this->ministate == 2) //Volume Down
            changestate(2, 0, &count);
    } else if (this->state == 3) {//Setting
        if (this->ministate == 0) { //Display Menu Setting
            if (input == 4) { //Set Frequency (A)
                changestate(4, 0, &count);
            } else if (input == 8) { //Set Key (B)
                if CHECKBIT(this->mode, SECURE_LOCK_FLAG)
                    changestate(3, 1, &count);
                else
                    changestate(5, 0, &count);
            } else if (input == 12) { //Set Power (B)
                changestate(6, 0, &count);
            } else if (input == 16) //Back (D)
                changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
        } else if (this->ministate == 1) { //Display "Secure this->mode lock"
            if (count > 20)
                changestate(3, 0, &count);
            else
                count++;
        }
    } else if (this->state == 4) {//frequency setting
        if (this->ministate == 0) { //Input Frequency
            if (input == 16) { //Back (D)
                changestate(3, 0, &count);
            } else if (input == 15) { //Enter (#)
                changestate(4, 2, &count);
            } else if (this->temp > 999999) {
                changestate(4, 1, &count);
            }
        } else if (this->ministate == 1) { //Display "Wrong Input"
            if (count > 20)
                changestate(4, 0, &count);
            else
                count++;
        } else if (this->ministate == 2) { //Send Setting to RF
            if (!CHECKBIT(this->mode, SET_FREQ_FLAG)) {
                if (this->setcode == SUCCESS) { //Setting Finished
                    changestate(4, 3, &count);
                } else if (this->setcode == INPUT_ERROR) { //Input Error
                    changestate(4, 1, &count);
                } else if (this->setcode == FAILED) { //Setting Failed
                    changestate(4, 4, &count);
                }
            }
        } else if (this->ministate == 3) { //Display "Setting Finished"
            if (count > 20)
                changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
            else
                count++;
        } else if (this->ministate == 4) { //Display "Setting Failed"
            if (count > 20) {
                changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
            } else
                count++;
        }
    } else if (this->state == 5) {//key setting
        if (this->ministate == 0) { //Select Host or Client
            if (input == 4) { //Host (A)
                changestate(5, 1, &count);
                SETBIT(this->mode, HOST_FLAG);
            } else if (input == 8) { //Client(A)
                changestate(5, 1, &count);
                CLEARBIT(this->mode, HOST_FLAG);
            } else if (input == 16) { //Back (D)
                changestate(CHECKBIT(this->mode, KEY_READY_FLAG) ? 3 : 0, 0, &count);
            }
        } else if (this->ministate == 1) { //Input PIN
            if (input == 16) { //Back (D)
                changestate(CHECKBIT(this->mode, KEY_READY_FLAG) ? 3 : 0, 0, &count);
            } else if (input == 15) { //Enter (#)
                /* Check PIN */
                if (cursor == 0) { //Blank PIN
                    changestate(5, 2, &count);
                } else if (this->mk_handle->input_password(this->temp)) { //Correct PIN
                    changestate(5, 4, &count);
                    this->pass_attempt = 1;
                } else if (this->pass_attempt % 5 != 0) { //Wrong PIN
                    /* Wrong PIN */
                    changestate(5, 3, &count);
                    this->pass_attempt++;
                } else { //Wrong PIN 5 Times
                    changestate(5, 11, &count);
                    this->pass_attempt++;
                }
            } else if (cursor > 8) { //Wrong PIN
                changestate(5, 3, &count);
            }
        } else if (this->ministate == 2) { //Display "Blank Input"
            if (count > 20) {
                changestate(5, 1, &count);
            } else
                count++;
        } else if (this->ministate == 3) { //Display "Wrong PIN"
            if (count > 20) {
                changestate(5, 1, &count);
            } else
                count++;
        } else if (this->ministate == 4) { //Input Key Number
            if (input == 16) { //Back (D)
                changestate(CHECKBIT(this->mode, KEY_READY_FLAG) ? 3 : 0, 0, &count);
            } else if (input == 15) { //Enter (#)
                if (this->index == 0)
                    changestate(5, 5, &count);
                else
                    changestate(5, 12, &count);
            } else if (this->index > 1000) { //Wrong Key Number
                changestate(5, 5, &count);
            }
        } else if (this->ministate == 5) { //Display "Wrong Key Number"
            if (count > 20) {
                changestate(5, 4, &count);
            } else
                count++;
        } else if (this->ministate == 6) { //Display "Send Key"
            if (input == 16) { //Back (D)
                changestate(1, 0, &count);
            } else if (input == 4) { //Send (A)
                changestate(5, 7, &count);
            }
        } else if (this->ministate == 7) { //Sending Key
            if (count > 20) {
                changestate(5, 6, &count);
            } else
                count++;
        } else if (this->ministate == 8) { //Receiving Key
            if (input == 16) { //Back (D)
                if CHECKBIT(this->mode, KEY_READY_FLAG) {
                    changestate(3, 0, &count);
                } else {
                    changestate(0, 0, &count);
                }
                CLEARBIT(this->mode, SET_KEY_FLAG);
            } else if (!CHECKBIT(this->mode, SET_KEY_FLAG)) {
                if (this->setcode == SUCCESS) {
                    changestate(5, 10, &count);
                } else if (this->setcode == FAILED) {
                    changestate(5, 9, &count);
                }
            } else
                count++;
        } else if (this->ministate == 9) { //invalid Key Received
            if (count > 20) {
                changestate(5, 8, &count);
            } else
                count++;
        } else if (this->ministate == 10) { //valid Key Received
            if (count > 20) {
                if CHECKBIT(this->mode, KEY_READY_FLAG) {
                    changestate(3, 0, &count);
                } else {
                    changestate(1, 0, &count);
                    SETBIT(this->mode, KEY_READY_FLAG);
                }
            } else
                count++;
        } else if (this->ministate == 11) { //Display "Secure Mode Locked"
            if (count > 20) {
                changestate(0, 0, &count);
            } else
                count++;
        } else if (this->ministate == 12) { //Preparing Key
            if (count > 20 && (!CHECKBIT(this->mode, GEN_KEY_FLAG))) {
                changestate(5, CHECKBIT(this->mode, HOST_FLAG) ? 6 : 8, &count);
            } else
                count++;
        }
    } else if (this->state == 6) { //Power Setting
        if (this->ministate == 0) { //Input Power
            if (input == 15) { //Enter (#)
                changestate(6, 1, &count);
            } else if (input == 16) { //Back (D)
                changestate(3, 0, &count);
            }
        } else if (this->ministate == 1) { //Send Setting to RF
            if (!CHECKBIT(this->mode, SET_POWER_FLAG)) {
                if (this->setcode == SUCCESS) { //Setting Finished
                    changestate(6, 2, &count);
                } else if (this->setcode == FAILED) {
                    changestate(6, 3, &count);
                }
            }
        } else if (this->ministate == 2) { //Display "Setting Finished"
            if (count > 20) {
                changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
            } else
                count++;
        } else if (this->ministate == 3) { //Display "Setting Failed"
            if (count > 20) {
                changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
            } else
                count++;
        }
    } else if (this->state == 7) {
        if (this->ministate == 0) { //pilih shutdown atau restart
            if (input == 4) {
                changestate(7, 1, &count);
            } else if (input == 8) {
                changestate(7, 3, &count);
            } else if (input == 16) //Back (D)
                changestate(CHECKBIT(this->mode, SECURE_FLAG) ? 1 : 0, 0, &count);
        } else if (this->ministate == 1) { //Display "goodbye"
            if (count > 20) {
                changestate(7, 2, &count);
            } else
                count++;
        } else if (this->ministate == 2) { //Shutdown system

        } else if (this->ministate == 3) { //Display "Restart"
            if (count > 20) {
                changestate(7, 4, &count);
            } else
                count++;
        } else if (this->ministate == 4) { //Restart system

        }
    }

    /*********************************************************/

    if (this->state == 0) {//not secured
        if (this->ministate == 0) {//Receive
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, SECURE_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Resume RF */
                this->rf_handle->resume();
                tcflush(this->rf_handle->handle(), TCIOFLUSH);
                /* Codec Sample Index */
                this->cd_handle->playback_resume();
                for (this->index = 0; this->index < SAMPLE_PER_FRAME; this->index++) {
                    this->buffer_rx[this->index] = SAMPLE_SILENCE;
                }
                this->index = 0;
                /* Display */
                int freq = this->rf_handle->getFrequency();
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Not Secured | RX");
                lcdPrintf(this->lcd_handle, "   %d KHz   ", freq);
            } else
                /* Pause capture while receiving */
                this->cd_handle->capture_pause();
        } else if (this->ministate == 1) { //Transmit
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, SECURE_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Codec */
                this->cd_handle->capture_resume();
                for (this->index = 0; this->index < SAMPLE_PER_FRAME; this->index++) {
                    this->buffer_tx[this->index] = SAMPLE_SILENCE;
                }
                this->index = 0;
                /* Display */
                int freq = this->rf_handle->getFrequency();
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Not Secured | TX");
                lcdPrintf(this->lcd_handle, "   %d KHz   ", freq);
            } else
                /* Pause capture while receiving */
                this->cd_handle->playback_pause();
        } else if (this->ministate == 2) { //Display "Secure this->mode lock"
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, SECURE_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Secure Mode   ");
                lcdPrintf(this->lcd_handle, "    Locked!     ");
            }
        }
    } else if (this->state == 1) { //secured
        if (this->ministate == 0) {//Receive
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                SETBIT(this->mode, SECURE_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Resume RF */
                this->rf_handle->resume();
                tcflush(this->rf_handle->handle(), TCIOFLUSH);
                /* Codec Sample Index */
                this->cd_handle->playback_resume();
                for (this->index = 0; this->index < SAMPLE_PER_FRAME; this->index++) {
                    this->buffer_rx[this->index] = SAMPLE_SILENCE;
                }
                this->index = 0;
                /* Display */
                int freq = this->rf_handle->getFrequency();
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "   Secured  | RX");
                lcdPrintf(this->lcd_handle, "   %d KHz   ", freq);
            } else
                /* Pause capture while receiving */
                this->cd_handle->capture_pause();
        } else { //Transmit
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, RX_FLAG);
                SETBIT(this->mode, SECURE_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Codec */
                this->cd_handle->capture_resume();
                for (this->index = 0; this->index < SAMPLE_PER_FRAME; this->index++) {
                    this->buffer_tx[this->index] = SAMPLE_SILENCE;
                }
                this->index = 0;
                /* Display */
                int freq = this->rf_handle->getFrequency();
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "   Secured  | TX");
                lcdPrintf(this->lcd_handle, "   %d KHz   ", freq);
            } else
                /* Pause capture while receiving */
                this->cd_handle->playback_pause();
        }
    } else if (this->state == 2) { //volume setting
        if (this->ministate == 0) { //Display Volume
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Get Volume */
                int vol = this->sc_handle->getVolume();
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Volume = %d %c  ", vol, 37);
                lcdPosition(this->lcd_handle, 0, 1);
                lcdPrintf(this->lcd_handle, " A. (+)  B. (-) ");
            }
        } else if (this->ministate == 1) { //Volume Up
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Increase Volume */
                this->sc_handle->increaseVolume();
            }
        } else if (this->ministate == 2) { //Volume Down
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Decrease Volume */
                this->sc_handle->decreaseVolume();
            }
        }
    } else if (this->state == 3) { //Setting
        if (this->ministate == 0) { //Display Menu Setting
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Resume RF */
                this->rf_handle->resume();
                tcflush(this->rf_handle->handle(), TCIOFLUSH);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "A.Freq  | B.Key ");
                lcdPrintf(this->lcd_handle, "C.Power | D.Back");
            }
        } else if (this->ministate == 1) { //Display "Secure mode lock"
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Secure Mode   ");
                lcdPrintf(this->lcd_handle, "    Locked!     ");
            }
        }
    } else if (this->state == 4) { //frequency setting
        if (this->ministate == 0) {
            char value = num(input);
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Pause RF While Setting */
                this->rf_handle->pause();
                /* frequency Temp Variable */
                this->temp = 0;
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Input Frequency=");
                lcdPosition(this->lcd_handle, 13, 1);
                lcdPrintf(this->lcd_handle, "KHz");
            } else if (value != 10) {
                /* Key Pressed */
                this->temp = (value == 11) ? (this->temp / 10) : (10 * this->temp) + value;
                /* Display */
                lcdPosition(this->lcd_handle, 0, 1);
                if (this->temp != 0)
                    lcdPrintf(this->lcd_handle, "%d ", this->temp);
                else
                    lcdPrintf(this->lcd_handle, " ");
            }
        } else if (this->ministate == 1) {
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Wrong Input!  ");
                lcdPrintf(this->lcd_handle, " 418 to 455 MHZ ");
            }
        } else if (this->ministate == 2) {
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, SET_FREQ_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Please Wait ...");
                /* Lock Mutex while accessing shared resource */
                pthread_mutex_lock(set_mutex);
                /* Signal finish_cond after 1 frame sampling */
                pthread_cond_signal(set_cond);
                /* Unlock Mutex */
                pthread_mutex_unlock(set_mutex);
            }
        } else if (this->ministate == 3) {
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Setting Finished");
            }
        } else if (this->ministate == 4) {
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, " Setting Failed");
            }
        }
    } else if (this->state == 5) { //key setting
        if (this->ministate == 0) { //Select Host or Client
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Pause RF While Setting */
                this->rf_handle->pause();
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Select Role   ");
                lcdPrintf(this->lcd_handle, "A.Host  B.Client");
            }
        } else if (this->ministate == 1) { //Input PIN
            char value = num(input);
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Key Number Temp Variable */
                this->temp = 0;
                /* Display */
                cursor = 0;
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Input PIN =");
            } else if (value != 10) {
                /* Key Pressed */
                if (value == 11) {
                    this->temp = ((cursor == 0) ? 0 : this->temp / 10);
                    cursor = ((cursor == 0) ? 0 : cursor - 1);
                } else if (cursor <= 8) {
                    this->temp = (10 * this->temp) + value;
                    cursor++;
                }
                /* Display */
                if (cursor != 0) {
                    lcdPosition(this->lcd_handle, cursor - 1, 1);
                    lcdPrintf(this->lcd_handle, "* ");
                } else {
                    lcdPosition(this->lcd_handle, 0, 1);
                    lcdPrintf(this->lcd_handle, " ");
                }
            }
        } else if (this->ministate == 2) { //Display "Blank input"
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Please Input PIN");
                lcdPrintf(this->lcd_handle, " (8 Digit PIN)  ");
            }
        } else if (this->ministate == 3) { //Display "Wrong PIN"
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "   Wrong PIN!   ");
                lcdPrintf(this->lcd_handle, " (8 Digit PIN)  ");
            }
        } else if (this->ministate == 4) { //Input Key Number
            char value = num(input);
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Key Number Temp Variable */
                this->index = 0;
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Key Number =");
            } else if (value != 10) {
                /* Key Pressed */
                this->index = (value == 11) ? (this->index / 10) : (10 * this->index) + value;
                /* Display */
                lcdPosition(this->lcd_handle, 0, 1);
                if (this->index != 0)
                    lcdPrintf(this->lcd_handle, "%d ", this->index);
                else
                    lcdPrintf(this->lcd_handle, " ");
            }
        } else if (this->ministate == 5) { //Display "Wrong Key Number"
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Wrong Input!  ");
                lcdPrintf(this->lcd_handle, "   (1 - 1000)   ");
            }
        } else if (this->ministate == 12) { //Preparing Key
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, GEN_KEY_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Preparing ");
                /* Lock Mutex while accessing shared resource */
                pthread_mutex_lock(set_mutex);
                /* Signal finish_cond after 1 frame sampling */
                pthread_cond_signal(set_cond);
                /* Unlock Mutex */
                pthread_mutex_unlock(set_mutex);
            } else if (count % 5 == 0)
                lcdPrintf(this->lcd_handle, ".");
        } else if (this->ministate == 6) { //Create Key / Ready to send
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Key Is Ready  ");
                lcdPrintf(this->lcd_handle, "Press A to Send!");
            }
        } else if (this->ministate == 7) { //Sending Key
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, SET_KEY_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Sending Key ");
                /* Lock Mutex while accessing shared resource */
                pthread_mutex_lock(set_mutex);
                /* Signal finish_cond after 1 frame sampling */
                pthread_cond_signal(set_cond);
                /* Unlock Mutex */
                pthread_mutex_unlock(set_mutex);
            } else if (count % 5 == 0)
                lcdPrintf(this->lcd_handle, ".");
        } else if (this->ministate == 8) { //Receiving Key
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, SET_KEY_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, " Searching Host ");
                lcdPrintf(this->lcd_handle, "Please Wait ");
                /* Lock Mutex while accessing shared resource */
                pthread_mutex_lock(set_mutex);
                /* Signal finish_cond after 1 frame sampling */
                pthread_cond_signal(set_cond);
                /* Unlock Mutex */
                pthread_mutex_unlock(set_mutex);
            } else if (count > 60) {
                /* Display */
                count = 0;
                lcdPosition(this->lcd_handle, 12, 1);
                lcdPrintf(this->lcd_handle, "    ");
                lcdPosition(this->lcd_handle, 12, 1);
            } else if (count % 15 == 0)
                /* Display */
                lcdPrintf(this->lcd_handle, ".");
        } else if (this->ministate == 9) { //Invalid Key Received
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Invalid Key   ");
                lcdPrintf(this->lcd_handle, "   Received     ");
            }
        } else if (this->ministate == 10) { //Valid Key Received
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Key Received  ");
                lcdPrintf(this->lcd_handle, "  Successfully  ");
            }
        } else if (this->ministate == 11) { //Secure Mode Locked
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, SECURE_LOCK_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "  Secure Mode   ");
                lcdPrintf(this->lcd_handle, "    Locked!     ");
                /* Lock Mutex while accessing shared resource */
                pthread_mutex_lock(lock_mutex);
                /* Signal finish_cond after 1 frame sampling */
                pthread_cond_signal(lock_cond);
                /* Unlock Mutex */
                pthread_mutex_unlock(lock_mutex);
            }
        }
    } else if (this->state == 6) { //Power Setting
        if (this->ministate == 0) { //Input Power
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, RX_FLAG);
                CLEARBIT(this->mode, TX_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Pause RF While Setting */
                this->rf_handle->pause();
                /* Power Temp Variable */
                this->temp = this->rf_handle->getPower();
                /* Display */
                lcdClear(this->lcd_handle);
                if (this->temp == 9) {
                    lcdPrintf(this->lcd_handle, "Power = 100 %c", 37);
                } else if (this->temp == 1) {
                    lcdPrintf(this->lcd_handle, "Power = 0 %c", 37);
                } else {
                    lcdPrintf(this->lcd_handle, "Power = %.1f %c", (float) (this->temp - 1) * 12.5, 37);
                }
                lcdPosition(this->lcd_handle, 0, 1);
                lcdPrintf(this->lcd_handle, " A. (+)  B. (-) ");
            } else if (input == 4) {
                /* Key Pressed */
                this->temp = (this->temp == 9) ? 9 : this->temp + 1;
                /* Display */
                lcdPosition(this->lcd_handle, 8, 0);
                if (this->temp < 9) {
                    lcdPrintf(this->lcd_handle, "%.1f %c", (float) (this->temp - 1) * 12.5, 37);
                } else {
                    lcdPrintf(this->lcd_handle, "100 %c ", 37);
                }
            } else if (input == 8) {
                /* Key Pressed */
                this->temp = (this->temp == 1) ? 1 : this->temp - 1;
                /* Display */
                lcdPosition(this->lcd_handle, 8, 0);
                if (this->temp > 1) {
                    lcdPrintf(this->lcd_handle, "%.1f %c", (float) (this->temp - 1) * 12.5, 37);
                } else {
                    lcdPrintf(this->lcd_handle, "0 %c   ", 37);
                }
            }
        } else if (this->ministate == 1) { //Send Setting To RF
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                SETBIT(this->mode, SET_POWER_FLAG);
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Please Wait ...");
                /* Lock Mutex while accessing shared resource */
                pthread_mutex_lock(set_mutex);
                /* Signal set_cond to do Setting in other thread */
                pthread_cond_signal(set_cond);
                /* Unlock Mutex */
                pthread_mutex_unlock(set_mutex);
            }
        } else if (this->ministate == 2) {
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Setting Finished");
            }
        } else if (this->ministate == 3) {
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, " Setting Failed ");
            }
        }
    } else if (this->state == 7) { //shutdown
        if (this->ministate == 0) { //pilih shutdown atau restart
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "A. Power Off    ");
                lcdPrintf(this->lcd_handle, "B. Restart      ");
            }
        } else if (this->ministate == 1) { //Display "goodbye"
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Turning Off ");
            } else if (count % 5 == 0)
                lcdPrintf(this->lcd_handle, ".");
        } else if (this->ministate == 2) { //Shutdown system
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                /* Shutdown system */
                system("sudo shutdown -h now");
                /* Exit Task */
                exit(0);
            }
        } else if (this->ministate == 3) { //Display "Restart"
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                lcdPrintf(this->lcd_handle, "Restarting ");
            } else if (count % 5 == 0)
                lcdPrintf(this->lcd_handle, ".");
        } else if (this->ministate == 4) { //Restart system
            if CHECKBIT(this->mode, STATE_CHANGED_FLAG) {
                /* Change Flag */
                CLEARBIT(this->mode, STATE_CHANGED_FLAG);
                /* Display */
                lcdClear(this->lcd_handle);
                /* Shutdown system */
                system("sudo reboot");
                /* Exit Task */
                exit(0);
            }
        }
    }
}

/* 
 * Processing Routine for Communication 
 * Executed Every 40mS
 * Encode -> Encrypt -> Transmit
 * Receive -> Decrypt -> Decode
 */
void controller::processing(pthread_mutex_t * sc_mutex, pthread_mutex_t * rf_mutex, pthread_cond_t * sc_cond, pthread_cond_t * rf_cond) {
    SAMPLE buf[SAMPLE_PER_FRAME];
    unsigned char id, compressed[BYTE_PER_FRAME];
    int i;

    if CHECKBIT(this->mode, RX_FLAG) {
        /* Receive */
        if (this->rf_handle->receive(this->cipher, sizeof (char) * SECURED_BYTE_PER_FRAME) == sizeof (char) * SECURED_BYTE_PER_FRAME) {
            /* Decrypt */
            if CHECKBIT(this->mode, SECURE_FLAG) {
                /* ---Security in here--- */
                if (this->srtp_handle->UnprotectVoicePacket(compressed, id, this->cipher) == 0)
                    /* Decode */
                    this->cd_handle->decode(buf, compressed);
                else
                    for (i = 0; i < SAMPLE_PER_FRAME; i++)
                        buf[i] = SAMPLE_SILENCE;
            } else {
                /* ---W/O Security here--- */
                for (i = 0; i < BYTE_PER_FRAME; i++)
                    compressed[i] = this->cipher[i + 3];
                /* Decode */
                this->cd_handle->decode(buf, compressed);
            }
        } else {
            /* Error handle */
            for (i = 0; i < SAMPLE_PER_FRAME; i++)
                buf[i] = SAMPLE_SILENCE;
        }
    }

    /* Lock Mutex */
    pthread_mutex_lock(sc_mutex);

    /* Release Mutex until finish_cond signal */
    pthread_cond_wait(sc_cond, sc_mutex);

    if CHECKBIT(this->mode, RX_FLAG) {
        /* Copy local buffer into share buffer */
        for (i = 0; i < SAMPLE_PER_FRAME; i++) {
            this->buffer_rx[i] = buf[i];
        }
    } else if CHECKBIT(this->mode, TX_FLAG) {
        /* Copy local buffer into share buffer */
        for (i = 0; i < SAMPLE_PER_FRAME; i++) {
            buf[i] = this->buffer_tx[i];
        }
    }

    /* Unlock Mutex */
    pthread_mutex_unlock(sc_mutex);

    if CHECKBIT(this->mode, TX_FLAG) {
        /* Do processing data while Transmit */
        /* Encode -> Encrypt -> Transmit */

        /* Encode */
        this->cd_handle->encode(buf, compressed);

        /* Encrypt */
        if CHECKBIT(this->mode, SECURE_FLAG) {
            /* ---Security in here--- */
            this->srtp_handle->ProtectVoicePacket(this->cipher, compressed);
        } else {
            /* ---W/O Security here--- */
            for (i = 0; i < BYTE_PER_FRAME; i++)
                this->cipher[i + 3] = compressed[i];
        }

        /* Transmit */
        /* Lock Mutex while accessing shared resource */
        pthread_mutex_lock(rf_mutex);
        /* Signal finish_cond after 1 frame sampling */
        pthread_cond_signal(rf_cond);
        /* Unlock Mutex */
        pthread_mutex_unlock(rf_mutex);
    }
}

/*
 * Sampling Routine for Communication
 * Executed in F Sampling = 8000Hz
 * Capture PCM
 * Playback PCM
 */
void controller::sampling(pthread_mutex_t * sc_mutex, pthread_cond_t * sc_cond) {
    if CHECKBIT(this->mode, TX_FLAG) {
        /* Transmit */

        /* Lock Mutex while accessing shared resource */
        pthread_mutex_lock(sc_mutex);
        if (this->index++ < SAMPLE_PER_FRAME) {
            /* Capture sample */
            this->buffer_tx[this->index - 1] = this->cd_handle->capture();
        } else {
            this->index = 0;
            /* Signal finish_cond after 1 frame sampling */
            pthread_cond_signal(sc_cond);
        }
        /* Unlock Mutex */
        pthread_mutex_unlock(sc_mutex);
    } else if CHECKBIT(this->mode, RX_FLAG) {
        /* Receive */

        /* Lock Mutex while accessing shared resource */
        pthread_mutex_lock(sc_mutex);
        if (this->index++ < SAMPLE_PER_FRAME) {
            /* Playback sample */
            this->cd_handle->playback(this->buffer_rx[this->index - 1]);
        } else {
            this->index = 0;
            /* Signal finish_cond after 1 frame sampling */
            pthread_cond_signal(sc_cond);
        }
        /* Unlock Mutex */
        pthread_mutex_unlock(sc_mutex);
    } else
        usleep(125);
}

/* Routine for transmitting */
void controller::transmitting(pthread_mutex_t * rf_mutex, pthread_cond_t * rf_cond) {
    unsigned char buf_cipher[SECURED_BYTE_PER_FRAME];
    int i;

    /* Lock Mutex */
    pthread_mutex_lock(rf_mutex);

    /* Release Mutex until finish_cond signal */
    pthread_cond_wait(rf_cond, rf_mutex);

    for (i = 0; i < SECURED_BYTE_PER_FRAME; i++) {
        buf_cipher[i] = this->cipher[i];
    }

    /* Unlock Mutex */
    pthread_mutex_unlock(rf_mutex);

    /* Transmit */
    for (i = 0; i < SECURED_BYTE_PER_FRAME; i++) {
        if (this->rf_handle->transmit(&buf_cipher[i], sizeof (char)) == sizeof (char))
            usleep(50);
    }
}

/* Routine for Setting */
void controller::setting(pthread_mutex_t * mutex, pthread_cond_t * cond) {
    int buf_temp;

    /* Lock Mutex */
    pthread_mutex_lock(mutex);

    /* Release Mutex until finish_cond signal */
    pthread_cond_wait(cond, mutex);

    /* Copy Shared buffer into local buffer */
    buf_temp = this->temp;

    /* Unlock Mutex */
    pthread_mutex_unlock(mutex);

    if CHECKBIT(this->mode, SET_FREQ_FLAG) {
        /* Set Frequency */
        this->setcode = this->rf_handle->setFrequency(buf_temp);
        /* Change Flag */
        CLEARBIT(this->mode, SET_FREQ_FLAG);
    } else if CHECKBIT(this->mode, SET_POWER_FLAG) {
        /* Set Power */
        this->setcode = this->rf_handle->setPower(buf_temp);
        /* Change Flag */
        CLEARBIT(this->mode, SET_POWER_FLAG);
    } else if CHECKBIT(this->mode, SET_KEY_FLAG) {
        /* Set Security Key */
        int i = 0;
        unsigned char secure_key_packet[42];
        session_key_t sessionkey_temp;

        /* Activate RF Communication */
        this->rf_handle->resume();
        usleep(100000);

        tcflush(this->rf_handle->handle(), TCIOFLUSH);
        if CHECKBIT(this->mode, HOST_FLAG) {
            /* Protect Key Packet */
            this->skt_handle->Protect(secure_key_packet, this->srtp_handle->getSessionKey());

            /* Sending Key */
            this->rf_handle->transmit(secure_key_packet, sizeof (char) * 42);
        } else {
            /* Receiving key */
            while ((i < 42) && CHECKBIT(this->mode, SET_KEY_FLAG)) {
                /* Receive Key */
                if (this->rf_handle->receive(&secure_key_packet[i], sizeof (char)) == sizeof (char)) {
                    i++;
                } else
                    /* Sleep */
                    usleep(500);
                if (i == 42) {
                    /* Decrypt Key Here */
                    this->setcode = this->skt_handle->Unprotect(sessionkey_temp, secure_key_packet);
                    if (this->setcode == SUCCESS)
                        this->srtp_handle->Init(this->mk_handle->getDeviceID(), sessionkey_temp);
                }
            }
        }
        /* Re-pause RF */
        usleep(200000);
        this->rf_handle->pause();
        tcflush(this->rf_handle->handle(), TCIOFLUSH);
        /* Change Flag */
        CLEARBIT(this->mode, SET_KEY_FLAG);
    } else if CHECKBIT(this->mode, GEN_KEY_FLAG) {
        /* Init SecKeyTransport */
        this->skt_handle->Init(this->mk_handle->getMasterKey(this->index - 1));
        if CHECKBIT(this->mode, HOST_FLAG) {
            session_key_t sessionkey_temp;

            /* Change Flag */
            SETBIT(this->mode, KEY_READY_FLAG);
            /* Create Session Key */
            this->mk_handle->generateRandom(sessionkey_temp.enc, 16);
            this->mk_handle->generateRandom(sessionkey_temp.salt, 16);
            /* Init SRTP */
            this->srtp_handle->Init(this->mk_handle->getDeviceID(), sessionkey_temp);
        }
        /* Change Flag */
        CLEARBIT(this->mode, GEN_KEY_FLAG);
    }
}

void controller::secure_lock(pthread_mutex_t * mutex, pthread_cond_t * cond) {
    int n;

    /* Lock Mutex */
    pthread_mutex_lock(mutex);

    /* Release Mutex until finish_cond signal */
    pthread_cond_wait(cond, mutex);

    /* Copy Shared buffer into local buffer */
    n = (this->pass_attempt / 5)*(this->pass_attempt / 5);

    /* Unlock Mutex */
    pthread_mutex_unlock(mutex);

    /* Lock in n minute */
    sleep(n * 60);

    /* Unlock Secure Mode */
    CLEARBIT(this->mode, SECURE_LOCK_FLAG);
}