/* 
 * File:   srtp.h
 * Author: gian nurman
 *
 * Created on 04 November 2013, 21:04
 */

#ifndef SRTP_H
#define	SRTP_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <assert.h>

#include "masterkey.h"

#include <cryptopp/osrng.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/filters.h>
#include <cryptopp/aes.h>
#include <cryptopp/ccm.h>
#include <cryptopp/hmac.h>
#include <cryptopp/sha.h>
#include <cryptopp/secblock.h>
#include <cryptopp/base64.h>

//define return value
#define INPUT_ERROR (-3)
#define FAILED (-2)
#define TIMEOUT (-1)
#define SUCCESS 0

//define return value for protect and unprotect packet
#define FAIL_SRRC -990
#define FAIL_INDEX -991
#define FAIL_SESSION_KEY_EXPIRED -992
#define FAIL_AUTH -992


#define SECURED_BYTE_PER_FRAME 20 

using namespace std;
using namespace CryptoPP;

typedef struct {
    byte enc[16];
    byte salt[16];
}
session_key_t;

typedef struct {
    byte enc[16];
    byte salt[16];
    byte auth[20];
}
subsession_key_t;

typedef struct {
    unsigned char enc[16];
    unsigned char salt[16];
    unsigned char auth[20];
}
master_key_t;

typedef struct {
    session_key_t session_key;
    unsigned char auth_tag[10];
}
key_packet_t;

typedef struct {
    word32 ROC;
    word16 SEQ;
}
ssrc_lib_t;

class Srtp {
public:
    Srtp();
    virtual ~Srtp();
    void Init(const byte ssrc_id, const session_key_t session_key);
    int ProtectVoicePacket(byte secure_voice_packet[20], const byte plain_payload[7]);
    int UnprotectVoicePacket(byte plain_payload[7], byte &ssrc, const byte secure_voice_packet[20]);
    session_key_t getSessionKey();

private:
    ssrc_lib_t ssrc_lib[256];
    byte ssrc_id;
    session_key_t session_key;
    subsession_key_t subsession_key;

    int KeyDerivator(subsession_key_t &subsession_key, const session_key_t session_key, const word64 kd_rate, const word64 indext);
    int SrtpInit(subsession_key_t &subsession_key, ssrc_lib_t ssrc_lib[256], const session_key_t session_key, const byte ssrc_id);
    int PrivatProtectVoicePacket(byte secure_voice_packet[20], ssrc_lib_t ssrc_lib[256], const byte ssrc_id, const byte plain_payload[7], const subsession_key_t subsession_key);
    int PrivatUnprotectVoicePacket(byte plain_payload[7], byte &ssrc, ssrc_lib_t ssrc_lib[256], const byte ssrc_id, const subsession_key_t subsession_key, const byte secure_voice_packet[20]);
};

#endif	/* SRTP_H */
