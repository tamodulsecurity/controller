/* 
 * File:   SecKeyTransport.h
 * Author: gian nurman
 *
 * Created on 05 November 2013, 14:42
 */


#ifndef SECKEYTRANSPORT_H
#define	SECKEYTRANSPORT_H

#include <iostream>
#include <string>
#include <cstdlib>
#include <assert.h>
#include <fstream>

#include <cryptopp/osrng.h>
#include <cryptopp/cryptlib.h>
#include <cryptopp/hex.h>
#include <cryptopp/filters.h>
#include <cryptopp/aes.h>
#include <cryptopp/ccm.h>
#include <cryptopp/hmac.h>
#include <cryptopp/sha.h>
#include <cryptopp/secblock.h>
#include <cryptopp/base64.h>

#include "Srtp.h"
#include "masterkey.h"
using namespace std;
using namespace CryptoPP;

class SecKeyTransport {
public:
    SecKeyTransport();
    virtual ~SecKeyTransport();

    int Init(master_key_t master_key);
    int Init(string master_key);
    int Init(byte enc_master_key[16], byte salt_master_key[16], byte auth_master_key[20]);

    int Protect(byte secure_key_packet_out[42], session_key_t session_key_in);
    int Protect(byte secure_key_packet_out[42], byte enc_session_key_in[16], byte salt_session_key_in[16]);
    byte* GetPacket() {
        return this->secure_key_packet;
    }
    
    int Unprotect(session_key_t &session_key_out, byte secure_key_packet_in[42]);
    int Unprotect(byte enc_session_key_out[16], byte salt_session_key_out[16], byte secure_key_packet_in[42]);
private:
    master_key_t master_key;
    byte secure_key_packet[42];
};

#endif	/* SECKEYTRANSPORT_H */

