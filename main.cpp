/* 
 * File:   main.cpp
 * Author: Fadhlullah
 * 
 * Created on 09 October 2013, 14:34
 */

/* Include standard c++ */
#include <cstdlib>
#include <cstdio>

/* Include Pthread library */
#include <pthread.h>

/* Include Object Class */
#include "rf_transceiver.h"
#include "soundcard.h"
#include "codec.h"
#include "controller.h"
#include "masterkey.h"
#include "SecKeyTransport.h"
#include "Srtp.h"
#include "Times.h"

using namespace std;

/* Define Thread Argument */
struct thread_arg {
    int index;
    long period_ns;
    controller * ctrl;
};

/* Define Variable for Pipeline */
pthread_mutex_t sc_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t rf_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t set_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lock_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t sc_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t rf_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t set_cond = PTHREAD_COND_INITIALIZER;
pthread_cond_t lock_cond = PTHREAD_COND_INITIALIZER;

/* Routine for Thread 6 */
void *Secure_lock(void *arg) {
    struct thread_arg *ps = (struct thread_arg *) arg;

    while (1) {
        /* Periodical Routine */
        ps->ctrl->secure_lock(&lock_mutex, &lock_cond);
    }
}

/* Routine for Thread 5 */
void *Setting(void *arg) {
    struct thread_arg *ps = (struct thread_arg *) arg;

    while (1) {
        /* Periodical Routine */
        ps->ctrl->setting(&set_mutex, &set_cond);
    }
}

/* Routine for Thread 4 */
void *Transmitting(void *arg) {
    struct thread_arg *ps = (struct thread_arg *) arg;

    while (1) {
        /* Periodical Routine */
        ps->ctrl->transmitting(&rf_mutex, &rf_cond);
    }
}

/* Routine for Thread 3 */
void *Processing(void *arg) {
    struct thread_arg *ps = (struct thread_arg *) arg;

    while (1) {
        /* Periodical Routine */
        ps->ctrl->processing(&sc_mutex, &rf_mutex, &sc_cond, &rf_cond);
    }
}

/* Routine for Thread 2 */
void *Sampling(void *arg) {
    struct thread_arg *ps = (struct thread_arg *) arg;

    while (1) {
        /* Periodical Routine */
        ps->ctrl->sampling(&sc_mutex, &sc_cond);
    }
}

/* Routine for Thread 1 */
void *Control(void *arg) {
    struct thread_arg *ps = (struct thread_arg *) arg;
    struct timespec next, now;

    clock_gettime(CLOCK_REALTIME, &next);
    while (1) {
        /* Periodical Routine */
        ps->ctrl->control(&set_mutex, &lock_mutex, &set_cond, &lock_cond);

        /* Deadline and Period Calculation */
        clock_gettime(CLOCK_REALTIME, &now);
        Times::timespec_add_ns(&next, ps->period_ns);
        if (Times::timespec_cmp(&now, &next) > 0) {
            printf("Deadline miss for thread %d\n", ps->index);
            printf("time: %ld nsec\n", Times::timespec_sub(&now, &next));
        } else
            clock_nanosleep(CLOCK_REALTIME, TIMER_ABSTIME, &next, NULL);
    }
}

int main(void) {
    /* Variable for Pthread */
    pthread_attr_t my_attr;
    pthread_t th1, th2, th3, th4, th5, th6;
    struct sched_param param1, param2, param3, param4, param5, param6;
    struct thread_arg arg1, arg2, arg3, arg4, arg5, arg6;

    /* Create a new object */
    codec * cdc = new codec();
    rf_transceiver * rf = new rf_transceiver();
    soundcard * sc = new soundcard();
    controller * ctrl = new controller();
    masterkey * mk = new masterkey();
    Srtp * srtp = new Srtp();
    SecKeyTransport * skt = new SecKeyTransport();

    /* Object Initialization */
    if (ctrl->init(rf, sc, cdc, mk, srtp, skt) == SUCCESS) {

        /* Initialization  */
        pthread_attr_init(&my_attr);

        /* Set FIFO as Scheduling policy */
        pthread_attr_setschedpolicy(&my_attr, SCHED_FIFO);

        /* Create Thread 1 */
        arg1.index = 1;
        arg1.period_ns = 50000000;
        arg1.ctrl = ctrl;
        param1.sched_priority = 10;
        pthread_attr_setschedparam(&my_attr, &param1);
        if (pthread_create(&th1, &my_attr, &Control, &arg1)) {
            printf("Thread1 creation failed!");
            exit(-1);
        }
        printf("Thread1 created \n");

        /* Create Thread 2 */
        arg2.index = 2;
        arg2.period_ns = 125000;
        arg2.ctrl = ctrl;
        param2.sched_priority = 1;
        pthread_attr_setschedparam(&my_attr, &param2);
        if (pthread_create(&th2, &my_attr, &Sampling, &arg2)) {
            printf("Thread2 creation failed!");
            exit(-1);
        }
        printf("Thread2 created \n");

        /* Create Thread 3 */
        arg3.index = 3;
        arg3.period_ns = 40000000;
        arg3.ctrl = ctrl;
        param3.sched_priority = 1;
        pthread_attr_setschedparam(&my_attr, &param3);
        if (pthread_create(&th3, &my_attr, &Processing, &arg3)) {
            printf("Thread3 creation failed!");
            exit(-1);
        }
        printf("Thread3 created \n");

        /* Create Thread 4 */
        arg4.index = 4;
        arg4.ctrl = ctrl;
        param3.sched_priority = 1;
        pthread_attr_setschedparam(&my_attr, &param4);
        if (pthread_create(&th4, &my_attr, &Transmitting, &arg4)) {
            printf("Thread4 creation failed!");
            exit(-1);
        }
        printf("Thread4 created \n");

        /* Create Thread 5 */
        arg5.index = 5;
        arg5.ctrl = ctrl;
        param4.sched_priority = 10;
        pthread_attr_setschedparam(&my_attr, &param5);
        if (pthread_create(&th5, &my_attr, &Setting, &arg5)) {
            printf("Thread5 creation failed!");
            exit(-1);
        }
        printf("Thread5 created \n");

        /* Create Thread 6 */
        arg6.index = 6;
        arg6.ctrl = ctrl;
        param4.sched_priority = 10;
        pthread_attr_setschedparam(&my_attr, &param6);
        if (pthread_create(&th6, &my_attr, &Secure_lock, &arg6)) {
            printf("Thread6 creation failed!");
            exit(-1);
        }
        printf("Thread6 created \n");

        /* Destroy Pthread Attribute Variable */
        pthread_attr_destroy(&my_attr);

        /* Join All Thread */
        pthread_join(th1, NULL);
        pthread_join(th2, NULL);
        pthread_join(th3, NULL);
        pthread_join(th4, NULL);
        pthread_join(th5, NULL);
        pthread_join(th6, NULL);
    } else {
        /* Object Initialization Failed */
        lcdClear(ctrl->lcd_handle);
        lcdPrintf(ctrl->lcd_handle, " ERROR SYS INIT ");
        sleep(1);
        lcdClear(ctrl->lcd_handle);
        lcdPrintf(ctrl->lcd_handle, " Turn off in  s ");
        for (int i = 5; i > 0; i--) {
            lcdPosition(ctrl->lcd_handle, 13, 0);
            lcdPrintf(ctrl->lcd_handle, "%d", i);
            sleep(1);
        }
        lcdClear(ctrl->lcd_handle);
        lcdPrintf(ctrl->lcd_handle, "Turning Off ");
        usleep(250000);
        lcdPrintf(ctrl->lcd_handle, ".");
        usleep(250000);
        lcdPrintf(ctrl->lcd_handle, ".");
        usleep(250000);
        lcdPrintf(ctrl->lcd_handle, ".");
        usleep(250000);
        lcdClear(ctrl->lcd_handle);
        system("sudo shutdown -h now");
        exit(0);
    }
}

