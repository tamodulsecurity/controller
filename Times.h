/* 
 * File:   Times.h
 * Author: Ifadh
 *
 * Created on 04 November 2013, 22:45
 */

#ifndef TIMES_H
#define	TIMES_H

#include <time.h>

class Times {
public:
    Times();
    Times(const Times& orig);
    virtual ~Times();
    static void timespec_add_ns(struct timespec *t, long ns); /* Add Time with nS  */
    static void timespec_add_minute(struct timespec *t, long minute); /* Add Time with minute  */
    static long timespec_sub(struct timespec *a, struct timespec *b); /* Calculate Difference Time Between a and b*/    
    static int timespec_cmp(struct timespec *a, struct timespec *b); /* Compare Time a and Time b */
private:

};

#endif	/* TIMES_H */

