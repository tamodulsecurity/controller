/* 
 * File:   rf_transceiver.h
 * Author: muhammad hasan
 *
 * Created on October 4, 2013, 7:23 PM
 */

#ifndef RF_TRANSCEIVER_H
#define	RF_TRANSCEIVER_H

//include c++ standard library
#include <cstdlib> 
#include <cstdio>
#include <iostream>
#include <cstring>
//include port communication library
#include <wiringPi.h>
#include "rs232.h"

//define baudrate & port number
#define UART_RATE 5
#define BAUD_RATE 38400
#define SET_BAUD_RATE 9600
#define RF_DATA_RATE 4
#define SERIAL_PORT 22
#define SET_PORT 1

//define return value
#define INPUT_ERROR (-3)
#define FAILED (-2)
#define TIMEOUT (-1)
#define SUCCESS 0

using namespace std;

//define rf transceiver class
class rf_transceiver {
public:
    rf_transceiver();
    virtual ~rf_transceiver();
    int init();
    int transmit(unsigned char * data,int data_length);
    int receive(unsigned char * data,int data_length);
    int setFrequency(int freq);
    int getFrequency();
    int setPower(int power);
    int getPower();
    int handle();
    void pause();
    void resume();
private:
    int changeRFSettings(int freq,int power);
    int fd;
    int frequency;
    int power;
};

#endif	/* RF_TRANSCEIVER_H */

