/* 
 * File:   Times.cpp
 * Author: Ifadh
 * 
 * Created on 04 November 2013, 22:45
 */

#include "Times.h"

Times::Times() {
}

Times::Times(const Times& orig) {
}

Times::~Times() {
}

/* Add Time with nS  */
void Times::timespec_add_ns(struct timespec *t, long ns) {
    t->tv_nsec += ns;
    if (t->tv_nsec > 1000000000) {
        t->tv_nsec = t->tv_nsec - 1000000000;
        t->tv_sec += 1;
    }
}


void Times::timespec_add_minute(struct timespec *t, long minute){
    t->tv_sec += 60 * minute;
}

/* Calculate Difference Time Between a and b*/
long Times::timespec_sub(struct timespec *a, struct timespec *b) {
    long int ret;

    ret = a->tv_nsec - b->tv_nsec;
    if (ret < 0) {
        ret += 1000000000;
    }
    return ret;
}

/* Compare Time a and Time b */
int Times::timespec_cmp(struct timespec *a, struct timespec *b) {
    if (a->tv_sec > b->tv_sec) return 1;
    else if (a->tv_sec < b->tv_sec) return -1;
    else if (a->tv_sec == b->tv_sec) {
        if (a->tv_nsec > b->tv_nsec) return 1;
        else if (a->tv_nsec == b->tv_nsec) return 0;
        else return -1;
    }
}

