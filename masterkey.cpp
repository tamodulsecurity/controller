/* 
 * File:   masterkey.cpp
 * Author: stei
 * 
 * Created on November 4, 2013, 2:13 PM
 */

#include "masterkey.h"

masterkey::masterkey() {
    this->password = "";
    string _key_location = UPLOAD_DIRECTORY;
    _key_location.append(MASTER_FILENAME);
    this->key_location = _key_location;
}

masterkey::masterkey(const masterkey& orig) {
}

masterkey::~masterkey() {
}

bool masterkey::input_password(int password_int) {
    std::stringstream pass;
    pass << password_int;
    this->password = pass.str();
    return this->check_password();
}

string masterkey::getMasterKey(long key_id) {
    //variable
    string master_hash, password_hash;
    fstream master_keys_file;

    //hashing the passwords
    password_hash = masterkey::sha_512(this->password);

    //open file to get the keys
    master_keys_file.open((this->key_location).c_str());

    if (key_id <= NUMBER_OF_KEYS && master_keys_file.is_open()) {
        // seek pointer to selected key id
        master_keys_file.seekg(key_id * CHIPER_LENGTH);

        //confirm current possition
        long current_pos = master_keys_file.tellg();

        //read next 64 bytes encrypte d key
        char * master_chipers = new char [CHIPER_LENGTH];
        master_keys_file.read(master_chipers,
                CHIPER_LENGTH);
        string chiper(master_chipers, CHIPER_LENGTH);

        //encoding data
        master_hash = masterkey::aes_cbc_decode(chiper, password_hash);

        //close handler
        master_keys_file.close();
    }

    return master_hash;
}

u_int8_t masterkey::getDeviceID() {
    //variable
    u_int8_t device_id;
    string device_id_s;
    string password_hash;
    fstream device_id_file;

    //hashing the passwords
    password_hash = masterkey::sha_512(DEVICE_ID_PASSWORD);

    //open file to get the keys
    string id_location = UPLOAD_DIRECTORY;
    id_location.append(DEVICE_ID_FILENAME);
    device_id_file.open(id_location.c_str());

    if (device_id_file.is_open()) {
        // seek pointer from beginning position
        device_id_file.seekg(0);

        //read next 32 bytes encrypted device id
        char * id_chiper = new char [ID_CHIPER_LENGTH];
        device_id_file.read(id_chiper,
                ID_CHIPER_LENGTH);
        string id_chipers(id_chiper, ID_CHIPER_LENGTH);

        //encoding data
        device_id_s = masterkey::aes_cbc_decode(id_chipers, password_hash);
        
        //assign 15th character
        device_id = (u_int8_t) device_id_s.c_str()[15];
        
        //close handler
        device_id_file.close();
    }

    return device_id;
}

byte * masterkey::parseEncryptionKey(string master_key) {
    byte * encryption_key = new byte[ENCRYPTION_KEY_LENGTH];
    string enc_s = master_key.substr(0, 16);
    encryption_key = enc_s.c_str();
    return encryption_key;
}

byte * masterkey::parseSaltKey(string master_key) {
    byte * salt_key = new byte[SALT_KEY_LENGTH];
    string salt_s = master_key.substr(16, 16);
    salt_key = salt_s.c_str();
    return salt_key;
}

byte * masterkey::parseAuthenticationKey(string master_key) {
    byte * authentication_key = new byte[AUTHENTICATION_KEY_LENGTH];
    string auth_s = master_key.substr(32, 20);
    authentication_key = auth_s.c_str();
    return authentication_key;
}

string masterkey::hex_encoder(string decoded) {
    string encoded;
    StringSource((byte *) decoded.data(), decoded.size(), true,
            new HexEncoder(
            new StringSink(encoded), true, 2, ":"
            )
            );
    return encoded;
}

string masterkey::hex_decoder(string encoded){
    string decoded;

    HexDecoder decoder;

    decoder.Attach( new StringSink( decoded ) );
    decoder.Put( (byte*)encoded.data(), encoded.size() );
    decoder.MessageEnd();

    return decoded;
}

bool masterkey::check_password() {
    //variable
    fstream master_keys_file;
    string crc_hash_key;

    //create crc hash
    string crc_hash = masterkey::sha_512(
            masterkey::master_key_hash(this->password));

    //open file to get the crc
    master_keys_file.open((this->key_location).c_str());

    if (master_keys_file.is_open()) {
        // seek pointer to selected key id
        master_keys_file.seekg(NUMBER_OF_KEYS * CHIPER_LENGTH);

        //read next 64 bytes of crc passwords
        char * crc_data = new char [CHIPER_LENGTH];
        master_keys_file.read(crc_data,
                CHIPER_LENGTH);
        string crc_data_s(crc_data, CHIPER_LENGTH);
        crc_hash_key = crc_data_s;

        //close handler
        master_keys_file.close();
    }

    if (crc_hash.compare(crc_hash_key) == 0) {
        return true;
    } else {
        return false;
    }
}

string masterkey::master_key_hash(string master_key) {
    string hash = sha_512(master_key);
    return hash.substr(12, 12 + MASTER_KEY_LENGTH);
}

string masterkey::sha_512(string source) {
    string result;
    SHA512 hash;

    //hashing source
    StringSource(source, true,
            new HashFilter(hash,
            new StringSink(result)));

    return result;
}

string masterkey::aes_cbc_decode(string cipher, string password_hash) {
    //attribute
    string key, iv;
    string master_key_h, salt;

    //convert salt into binary string
    salt = AES_CBC_SALT;
    StringSource ss(salt,
            new HexDecoder(
            new StringSink(iv)
            )
            );
    //copy password hash to key for encoding
    key = password_hash;

    //decryption
    try {
        CBC_Mode< AES >::Decryption dec;
        dec.SetKeyWithIV(key.data(), AES_CBC_KEY_LENGTH, iv.data());
        StringSource ss(cipher, true,
                new StreamTransformationFilter(dec,
                new StringSink(master_key_h)
                )
                );
    } catch (const CryptoPP::Exception& e) {
        cerr << e.what() << endl;
        exit(-1);
    }

    return master_key_h;
}

void masterkey::generateRandom(byte * data, int size)
{
    ifstream rand_dev("/dev/hwrng", ios::in | ios::binary);
    rand_dev.read(data, size);
    rand_dev.close();
}
