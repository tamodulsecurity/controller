/* 
 * File:   soundcard.h
 * Author: muhammad hasan 
 *
 * Created on October 4, 2013, 7:21 PM
 */

#ifndef SOUNDCARD_H
#define	SOUNDCARD_H

#include <cstdlib>
#include <cstdio>
#include <alsa/asoundlib.h> //libsound library
#include <alsa/mixer.h> //include alsa mixer library

#define MIN_VOLUME 0
#define MAX_VOLUME 100

//define return value
#define INPUT_ERROR (-3)
#define FAILED (-2)
#define TIMEOUT (-1)
#define SUCCESS 0

class soundcard {
public:
    long min;
    long max;
    soundcard();
    soundcard(const soundcard& orig);
    virtual ~soundcard();
    int init();
    int getVolume();
    int increaseVolume();
    int decreaseVolume();
    int increaseVolume(int factor);
    int decreaseVolume(int factor);
    int setVolume(int _volume);
private:
    int volume;
    const char * card;
    const char * selem_name;
    snd_mixer_elem_t* elem;
    snd_mixer_t *handle;
    snd_mixer_selem_id_t *sid;
};

#endif	/* SOUNDCARD_H */

