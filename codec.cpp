/* 
 * File:   codec.cpp
 * Author: muhammad hasan
 * 
 * Created on October 4, 2013, 7:21 PM
 */

#include "codec.h" 

codec::codec() {
    //init codec-2 configurations
    this->codec2_handle = codec2_create(CODEC2_MODE_1400);
    this->enable = true;
    this->sampling_rate = SAMPLE_RATE;
    this->dir = 0;
    this->hw_playback_name = "internal";
    this->hw_capture_name = "default";
}

codec::~codec() {
}

//init codec
int codec::init() {
    //local vars
    int err;

    //init playback & record parameters
    //open device
    if ((err = snd_pcm_open(&this->capture_handle, this->hw_capture_name, SND_PCM_STREAM_CAPTURE, 0)) < 0)
        return FAILED;
    if ((err = snd_pcm_open(&this->playback_handle, this->hw_playback_name, SND_PCM_STREAM_PLAYBACK, 0)) < 0)
        return FAILED;

    //setup capture parameters
    if ((err = snd_pcm_hw_params_malloc(&this->capture_params)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_any(this->capture_handle, this->capture_params)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_access(this->capture_handle, this->capture_params,
            SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_format(this->capture_handle, this->capture_params,
            SND_PCM_FORMAT_S16_LE)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_rate_near(this->capture_handle, this->capture_params,
            &this->sampling_rate, &this->dir)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_channels(this->capture_handle, this->capture_params,
            HW_CHANNEL)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params(this->capture_handle, this->capture_params)) < 0)
        return FAILED;
    snd_pcm_hw_params_free(this->capture_params);

    //setup playback parameters
    if ((err = snd_pcm_hw_params_malloc(&this->playback_params)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_any(this->playback_handle, this->playback_params)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_access(this->playback_handle, this->playback_params,
            SND_PCM_ACCESS_RW_INTERLEAVED)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_format(this->playback_handle, this->playback_params,
            SND_PCM_FORMAT_S16_LE)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_rate_near(this->playback_handle, this->playback_params,
            &sampling_rate, &dir)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params_set_channels(this->playback_handle, this->playback_params,
            HW_CHANNEL)) < 0)
        return FAILED;
    if ((err = snd_pcm_hw_params(this->playback_handle, this->playback_params)) < 0)
        return FAILED;
    snd_pcm_hw_params_free(this->playback_params);

    //prepare devices
    if ((err = snd_pcm_prepare(this->capture_handle)) < 0)
        return FAILED;
    if ((err = snd_pcm_prepare(this->playback_handle)) < 0)
        return FAILED;

    //return success
    return SUCCESS;
}

//speak to speaker

void codec::playback(SAMPLE decompressed) {
    int err;
    //write data to playback device
    if ((err = snd_pcm_writei(this->playback_handle, (const void *) &decompressed, 1)) != 1) {
        //error handling
        switch (err) {
            case -EBADFD: cout << "pcm not in the right state" << std::endl;
                snd_pcm_recover(this->playback_handle, err, SAMPLE_SILENCE);
                break;
            case -EPIPE: cout << "overrun" << std::endl;
                snd_pcm_recover(this->playback_handle, err, SAMPLE_SILENCE);
                break;
            case -ESTRPIPE: cout << "suspend event ocurred" << std::endl;
                snd_pcm_recover(this->playback_handle, err, SAMPLE_SILENCE);
                break;
            default:
                cout << "unknown error ocurred " << err << std::endl;
                //check state of pcm handler
                snd_pcm_state_t st = snd_pcm_state(this->playback_handle);
                cout << "current state is " << st << std::endl
                        << "\n try to recover system.."
                        << std::endl;
                switch (st) {
                    case SND_PCM_STATE_XRUN:
                        snd_pcm_recover(this->playback_handle, -EPIPE, SAMPLE_SILENCE);
                        break;
                    case SND_PCM_STATE_PREPARED:
                        snd_pcm_recover(this->playback_handle, -EBADFD, SAMPLE_SILENCE);
                        break;
                    case SND_PCM_STATE_SUSPENDED:
                        snd_pcm_recover(this->playback_handle, -ESTRPIPE, SAMPLE_SILENCE);
                        break;
                    default:
                        cout << "system cannot recover error playback" << std::endl;
                        system("sudo /home/pi/secure-2-way-radio/keep-alive &");
                        exit(-1);
                }
        }
    }
}

//encoding - decoding

void codec::decode(SAMPLE decompressed[], unsigned char compressed[]) {
    codec2_decode(codec2_handle, decompressed, compressed);
}

void codec::encode(SAMPLE uncompressed[], unsigned char compressed[]) {
    //encode Data
    codec2_encode(codec2_handle, compressed, uncompressed);

}

//listen to microphone

SAMPLE codec::capture() {
    //allocate buffers
    SAMPLE sample;
    int err;

    //read captured data
    if ((err = snd_pcm_readi(this->capture_handle, (void *) &sample, 1)) == 1) {
        return sample;
    } else {
        switch (err) {
            case -EBADFD: cout << "pcm not in the right state" << std::endl;
                snd_pcm_recover(this->capture_handle, err, SAMPLE_SILENCE);
                break;
            case -EPIPE: cout << "overrun" << std::endl;
                snd_pcm_recover(this->capture_handle, err, SAMPLE_SILENCE);
                break;
            case -ESTRPIPE: cout << "suspend event ocurred" << std::endl;
                snd_pcm_recover(this->capture_handle, err, SAMPLE_SILENCE);
                break;
            default:
                cout << "unknown error ocurred " << err << std::endl;
                snd_pcm_state_t st = snd_pcm_state(this->capture_handle);
                cout << "current state is " << st << std::endl
                        << "\n try to recover system.."
                        << std::endl;
                switch (st) {
                    case SND_PCM_STATE_XRUN:
                        snd_pcm_recover(this->capture_handle, -EPIPE, SAMPLE_SILENCE);
                        break;
                    case SND_PCM_STATE_PREPARED:
                        snd_pcm_recover(this->capture_handle, -EBADFD, SAMPLE_SILENCE);
                        break;
                    case SND_PCM_STATE_SUSPENDED:
                        snd_pcm_recover(this->capture_handle, -ESTRPIPE, SAMPLE_SILENCE);
                        break;
                    default:
                        cout << "system cannot recover error capture" << std::endl;
                        system("sudo /home/pi/secure-2-way-radio/keep-alive &");
                        exit(-1);
                }
        }
        return SAMPLE_SILENCE;
    }
}


void codec::playback_resume() {
    snd_pcm_prepare(this->playback_handle);
}

void codec::playback_pause() {
    snd_pcm_drop(this->playback_handle);
}

void codec::capture_resume() {
    snd_pcm_prepare(this->capture_handle);
}

void codec::capture_pause() {
    snd_pcm_drop(this->capture_handle);
}

void codec::terminate() {
    //close sound card handlers
    snd_pcm_close(this->capture_handle);
    snd_pcm_close(this->playback_handle);
}