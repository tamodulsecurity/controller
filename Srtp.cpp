/* 
 * File:   srtp.cpp
 * Author: gian nurman
 * 
 * Created on 04 November 2013, 21:04
 */

#include "Srtp.h"

Srtp::Srtp() {
}

Srtp::~Srtp() {
}

void Srtp::Init(const byte ssrc_id, const session_key_t session_key) {
    this->ssrc_id = ssrc_id;
    this->session_key = session_key;
    SrtpInit(this->subsession_key, this->ssrc_lib, session_key, ssrc_id);
}

session_key_t Srtp::getSessionKey() {
    return this->session_key;
}

int Srtp::ProtectVoicePacket(byte secure_voice_packet[20], const byte plain_payload[7]) {
    return PrivatProtectVoicePacket(secure_voice_packet, this->ssrc_lib, this->ssrc_id, plain_payload, this->subsession_key);
}

int Srtp::UnprotectVoicePacket(byte plain_payload[7], byte &ssrc, const byte secure_voice_packet[20]) {
    return PrivatUnprotectVoicePacket(plain_payload, ssrc, this->ssrc_lib, this->ssrc_id, this->subsession_key, secure_voice_packet);
}

int Srtp::SrtpInit(subsession_key_t &subsession_key, ssrc_lib_t ssrc_lib[256], const session_key_t session_key, const byte ssrc_id) {
    //KDF    
    int status;
    int j;

    status = KeyDerivator(subsession_key, session_key, 0xFF, 0x00);
    if (status != SUCCESS) {
        return FAILED;
    }

    for (j = 0; j < 256; j++) {
        ssrc_lib[j].ROC = 0;
        ssrc_lib[j].SEQ = 0;
    }
    
    byte random[2];
    masterkey::generateRandom(random, 2);
    word16 temp1 = random[0];
    word16 temp2 = random[1];
    ssrc_lib[ssrc_id].SEQ = (temp1 << 8) + temp2;

    return SUCCESS;
}

int Srtp::KeyDerivator(subsession_key_t &subsession_key, const session_key_t session_key, const word64 kd_rate, const word64 indext) {
    int i;
    SecByteBlock IV(0x00, 16);
    SecByteBlock x(0x00, 16);
    SecByteBlock cipher(0x00, 16);
    word64 r = indext / kd_rate; //indext: 48-bit

    for (i = 13; i > 7; i--) {
        r = r >> 8;
        x[i] = r;
    }

    x[7] = 0x00;
    for (i = 0; i < 14; i++) {
        IV[i] = x[i] ^ session_key.salt[i];
    }

    CTR_Mode<AES>::Encryption encryptor;
    encryptor.SetKeyWithIV(session_key.enc, 16, IV);
    encryptor.ProcessString(cipher, 16);
    for (i = 0; i < 16; i++) {
        subsession_key.enc[i] = cipher[i];
    }

    cipher.CleanNew(16);
    x.CleanNew(16);
    x[7] = 0x02;
    for (i = 0; i < 14; i++) {
        IV[i] = x[i] ^ session_key.salt[i];
    }
    encryptor.SetKeyWithIV(session_key.enc, 16, IV);
    //encryptor.ProcessData(secure_payload, plain_payload, 7);
    encryptor.GenerateBlock(cipher, 16);
    for (i = 0; i < 16; i++) {
        subsession_key.salt[i] = cipher[i];
    }

    cipher.CleanNew(20);
    x.CleanNew(16);
    x[7] = 0x01;
    for (i = 0; i < 16; i++) {
        IV[i] = x[i] ^ session_key.salt[i];
    }
    encryptor.SetKeyWithIV(session_key.enc, 16, IV);
    //encryptor.ProcessData(secure_payload, plain_payload, 7);
    encryptor.GenerateBlock(cipher, 20);
    for (i = 0; i < 20; i++) {
        subsession_key.auth[i] = cipher[i];
    }

    return SUCCESS;
}

int Srtp::PrivatProtectVoicePacket(byte secure_voice_packet[20], ssrc_lib_t ssrc_lib[256], const byte ssrc_id, const byte plain_payload[7], const subsession_key_t subsession_key) {
    int i = 0;
    byte secure_payload[7];

    word64 SEQ = ssrc_lib[ssrc_id].SEQ;
    word64 ROC = ssrc_lib[ssrc_id].ROC;

    secure_voice_packet[0] = ssrc_id;
    secure_voice_packet[1] = SEQ >> 8;
    secure_voice_packet[2] = SEQ;

    SecByteBlock IV(0x00, 16);

    IV[7] = ssrc_id;
    word64 temp = SEQ;
    for (i = 13; i > 11; i--) {
        IV[i] = temp;
        temp = temp >> 8;
    }
    temp = ROC;
    for (i = 11; i > 7; i--) {
        IV[i] = temp;
        temp = temp >> 8;
    }


    //    cout << '\t' << IV[7] << endl; 

    //    string encoded;
    //    encoded.clear();
    //        StringSource(IV, 16, true,
    //                new HexEncoder(
    //                        new StringSink(encoded)
    //                ) // HexEncoder
    //        ); // StringSource
    //    cout << "\rIV\t\t" << encoded << endl;

    for (i = 0; i < 16; i++) {
        IV[i] = IV[i] ^ subsession_key.salt[i];
    }

    //    encoded.clear();
    //        StringSource(IV, 16, true,
    //                new HexEncoder(
    //                        new StringSink(encoded)
    //                ) // HexEncoder
    //        ); // StringSource
    //    cout << "\rIV\t\t" << encoded << endl;

    CTR_Mode<AES>::Encryption encryptor;
    encryptor.SetKeyWithIV(subsession_key.enc, 16, IV);
    encryptor.ProcessData(secure_payload, plain_payload, 7);

    for (i = 0; i < 7; i++) {
        secure_voice_packet[i + 3] = secure_payload[i];
    }

    byte hash[10];
    HMAC<SHA1> hmac(subsession_key.auth, 20);
    hmac.CalculateTruncatedDigest(hash, 10, secure_voice_packet, 10);

    for (i = 0; i < 10; i++) {
        secure_voice_packet[i + 10] = hash[i];
    }

    if (SEQ == 0xFFFF) {
        SEQ = 0;
        if (ROC == 0xFFFFFFFF) {
            return 0;
            //session_key kadaluarsa
        } else {
            ROC++;
        }
    } else {
        SEQ++;
    }
    ssrc_lib[ssrc_id].ROC = ROC;
    ssrc_lib[ssrc_id].SEQ = SEQ;

    return SUCCESS;
}

int Srtp::PrivatUnprotectVoicePacket(byte plain_payload[7], byte &ssrc, ssrc_lib_t ssrc_lib[256], const byte ssrc_id, const subsession_key_t subsession_key, const byte secure_voice_packet[20]) {
    int i = 0;
    byte secure_payload[7];

    byte packet_ssrc = secure_voice_packet[0];
    ssrc = packet_ssrc;
    if (ssrc_id == packet_ssrc) {
        return FAIL_SRRC;
    }

    word64 ROC = ssrc_lib[packet_ssrc].ROC;
    word64 SEQ = ssrc_lib[packet_ssrc].SEQ;
    //    cout << "\rUnprotect\t" << ROC << "\t" << SEQ << endl;

    word64 packet_seq; //packet_seq: 16-bit diproses:64
    word64 temp1 = secure_voice_packet[1];
    word64 temp2 = secure_voice_packet[2];
    packet_seq = (temp1 << 8) + temp2;

    word64 packet_indext[2];
    packet_indext[0] = (((ROC) && 0x0000000000000000FFFFFFFFFFFFFFFF) << 16) + packet_seq;
    packet_indext[1] = (((ROC + 1) && 0x0000000000000000FFFFFFFFFFFFFFFF) << 16) + packet_seq;

    word64 IDX = (ROC << 16) + SEQ;
    //    cout << "\rUnprotect\t" << IDX << "\t" << packet_seq << endl;

    if (packet_indext[0] == IDX) {
        //Pakai IDX yang telah dihitung
    } else if (packet_indext[0] < IDX) {
        return FAIL_INDEX;
    } else if (packet_indext[0] > IDX) {
        IDX = packet_indext[0];
    } else if (packet_indext[1] > IDX) {
        IDX = packet_indext[1];
    }

    //    cout << "\rUnprotect\t" << SEQ << "\t" << packet_seq << "\t" << packet_indext[0] << "\t" << packet_indext[1] << "\t" << IDX << endl;

    byte hash[10];
    for (i = 0; i < 10; i++) {
        hash[i] = secure_voice_packet[i + 10];
    }

    HMAC< SHA1 > hmac(subsession_key.auth, 20);
    if (hmac.VerifyTruncatedDigest(hash, 10, secure_voice_packet, 10)) {
        for (i = 0; i < 7; i++) {
            secure_payload[i] = secure_voice_packet[i + 3];
        }

        SecByteBlock IV(0x00, 16);

        word64 temp = IDX;
        for (i = 13; i > 7; i--) {
            IV[i] = temp;
            temp = temp >> 8;
        }
        IV[7] = ssrc; //ssrc paket

        //        string encoded;
        //        encoded.clear();
        //        StringSource(IV, 16, true,
        //                new HexEncoder(
        //                        new StringSink(encoded)
        //                ) // HexEncoder
        //        ); // StringSource
        //        cout << "\rIV\t\t" << encoded << endl;

        for (i = 0; i < 16; i++) {
            IV[i] = IV[i] ^ subsession_key.salt[i];
        }

        //        encoded.clear();
        //        StringSource(IV, 16, true,
        //                new HexEncoder(
        //                        new StringSink(encoded)
        //                ) // HexEncoder
        //        ); // StringSource
        //        cout << "\rIV\t\t" << encoded << endl;

        CTR_Mode< AES >::Decryption decryptor;
        decryptor.SetKeyWithIV(subsession_key.enc, 16, IV);
        byte temp_payload[7];
        decryptor.ProcessData(temp_payload, secure_payload, 7);
        for (i = 0; i < 7; i++) {
            plain_payload[i] = temp_payload[i];
            //            plain_payload[i]= secure_payload[i];
        }

        SEQ = IDX;
        ROC = IDX >> 16;
        if (SEQ == 0xFFFF) {
            SEQ = 0;
            if (ROC == 0xFFFFFFFF) {
                return FAIL_SESSION_KEY_EXPIRED;
                //session_key kadaluarsa
            } else {
                ROC++;
            }
        } else {
            SEQ++;
        }
        ssrc_lib[packet_ssrc].ROC = ROC;
        ssrc_lib[packet_ssrc].SEQ = SEQ;
        return SUCCESS;
    } else {
        for (i = 0; i < 7; i++) {
            plain_payload[i] = 0;
        }
        return FAIL_AUTH;
    }
}

