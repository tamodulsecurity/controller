/* 
 * File:   soundcard.cpp
 * Author: stei
 * 
 * Created on October 4, 2013, 7:21 PM
 */

#include "soundcard.h"
#include <stdlib.h>
#include <iostream>

using namespace std;

soundcard::soundcard() {
    //init all attribute value
    this->min = 0;
    this->max = 1;
    this->card = "hw:0";
    this->volume = 70;
    this->selem_name = "PCM";
}

soundcard::soundcard(const soundcard& orig) {
}

soundcard::~soundcard() {
}

int soundcard::init() {
    int err = 0;

    //create handler
    snd_mixer_open(&this->handle, 0);
    snd_mixer_attach(this->handle, this->card);
    snd_mixer_selem_register(this->handle, NULL, NULL);
    err = snd_mixer_load(this->handle);
    if (err > 0) {
        printf("%d", err);
        snd_mixer_close(this->handle);
        return FAILED;
    }

    //create mixer object
    snd_mixer_selem_id_alloca(&this->sid);
    snd_mixer_selem_id_set_index(this->sid, 0);
    snd_mixer_selem_id_set_name(this->sid, this->selem_name);
    this->elem = snd_mixer_find_selem(this->handle, this->sid);
    if (!this->elem) {
        printf("mixer not found");
        return FAILED;
    }

    //get volume range
    if (snd_mixer_selem_has_playback_volume(this->elem)) {
        snd_mixer_selem_get_playback_volume_range(this->elem, &this->min, &this->max);
        return this->setVolume(this->volume);
    } else {
        return FAILED;
    }

    return SUCCESS;
}

//set sound card volume

int soundcard::setVolume(int _volume) {
    if (_volume <= 100 && _volume >= 0) {
        snd_mixer_selem_set_playback_volume_all(
                this->elem,
                (int) 
                (this->min + 
                ((this->max - this->min) * 0.6) + 
                ((this->max - this->min) * 0.4 * ((float) _volume / 100.00))));
        this->volume = _volume;
        return SUCCESS;
    } else {
        printf("volume out of range");
        return FAILED;
    }
}

int soundcard::getVolume() {
    return this->volume;
}

//increase volumes

int soundcard::increaseVolume(int factor) {
    int new_volume = this->volume + factor;
    if (new_volume <= 100) {
        this->setVolume(new_volume);
        return new_volume;
    }
    return this->volume;
}

int soundcard::increaseVolume() {
    return this->increaseVolume(10);
}

//decrease volumes

int soundcard::decreaseVolume(int factor) {
    int new_volume = this->volume - factor;
    if (new_volume >= 0) {
        this->setVolume(new_volume);
    }
    return this->volume;
}

int soundcard::decreaseVolume() {
    return this->decreaseVolume(10);
}


