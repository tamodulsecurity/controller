/* 
 * File:   codec.h
 * Author: muhammad hasan 
 *
 * Created on October 4, 2013, 7:21 PM
 */

//include standard c++ 
#include <cstdlib>
#include <cstdio> 
//import audio related library
#include <alsa/asoundlib.h>
#include <codec2.h>
//include RF Transceiver
#include "rf_transceiver.h"

#ifndef CODEC_H
#define	CODEC_H

//Select sample & hardware sound format
#define SAMPLE_RATE  (8000)
#define SAMPLE_SILENCE  (0)
#define HW_CHANNEL (1)
#define BYTE_PER_FRAME (7)
#define SAMPLE_PER_FRAME (320)
#define BIT_PER_FRAME (56)
typedef short SAMPLE;

//define return value
#define INPUT_ERROR (-3)
#define FAILED (-2)
#define TIMEOUT (-1)
#define SUCCESS 0

//define class codec

class codec {
public:
    codec(); //cc
    virtual ~codec();
    int init(); //init codec
    void playback(SAMPLE decompressed); //start playing
    SAMPLE capture(); //start listening
    void decode(SAMPLE decompressed[], unsigned char compressed[]);
    void encode(SAMPLE uncompressed[], unsigned char compressed[]);
    void playback_resume();
    void playback_pause();
    void capture_resume();
    void capture_pause();
    void terminate(); //terminate codec process
private:
    CODEC2 * codec2_handle;
    snd_pcm_t *capture_handle;
    snd_pcm_t *playback_handle;
    snd_pcm_hw_params_t *capture_params;
    snd_pcm_hw_params_t *playback_params;
    bool enable;
    unsigned int sampling_rate;
    int dir;
    const char * hw_playback_name;
    const char * hw_capture_name;
};

#endif	/* CODEC_H */
